# -*- coding: utf-8 -*-

from odoo import models, fields


class Jurusan(models.Model):
    _name = "mahasiswa.jurusan"

    name = fields.Char('Nama Jurusan', required=True)
    gedung = fields.Integer('Gedung', required=True)
    mahasiswa_ids = fields.One2many("mahasiswa.mahasiswa", "jurusan_id", 'List Mahasiswa')
    npm_prefix = fields.Integer('NPM', required = True)
