# -*- coding: utf-8 -*-

from odoo import models, fields, api, exceptions, _

SELECTION_GENDER = [
    ("male", "Male"),
    ("female", "Female")
]

class Mahasiswa(models.Model):
    _name = "mahasiswa.mahasiswa"

    @api.model
    def domain_jurusan_id(self):
        return [('id', '=', 1)]

    name = fields.Char()
    npm = fields.Char('NPM', required=True)
    jurusan_id = fields.Many2one("mahasiswa.jurusan", "Jurusan")
    gender = fields.Selection(SELECTION_GENDER, "Gender")

    @api.multi
    def write(self, values):#untuk parameter write pake values
        self.cekInput(values=values)
        jurusan_obj = self.env.get('mahasiswa.jurusan')#self.pool.get(mahasiswa.jurusan) untuk memanggil method&atribut dri kelas lain di tabel mahasiswa.jurusan

        result = super(Mahasiswa, self).write(values)
        return result

    @api.model
    def create(self, vals):#untuk parameter create pake vals
        self.cekInput(values=vals)
        result = super(Mahasiswa, self).create(vals)
        return result

    @api.model
    def cekInput(self, values):
        if values.get('npm', False) and len(values['npm'])<=5:
            raise exceptions.except_orm(_('Mahasiswa Error'), _('NPM must be longer than 5 characters.'))
        if values.get('jurusan_id', False):#Kalau diedit user input nama aja tpi npm nggak diganti, klo nggak di cek error
            jurusan_obj = self.env.get('mahasiswa.jurusan')
            jurusan_datas = jurusan_obj.browse([values['jurusan_id']])
            # jurusan_obj.search([
            #     ('id','=',values['jurusan_id']),
            #     ('gedung', '>', 0)
            # ])[0]
            if len(jurusan_datas)>0 and jurusan_datas[0].gedung== 0:
                raise exceptions.except_orm(_('Mahasiswa Error'), _('Gedung must not 0.'))

    @api.onchange('jurusan_id')
    def onchange_jurusan_id(self):

        return {
            'value': {
                'npm': self.jurusan_id.npm_prefix,
                'name': 'bob'
            }
        }