# -*- coding: utf-8 -*-
{
    'name': "Mahasiswa",

    'summary': """
        Test 
    """,

    'description': """
        test2
    """,

    'author': "eldon",
    'website': "http://www.facebook.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'board', 'web', 'mail'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'menus/mahasiswa_menu.xml',
        'views/mahasiswa_view.xml',
        'views/jurusan_view.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        # 'demo/demo.xml',
    ],
}
